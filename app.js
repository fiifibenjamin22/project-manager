const createError = require('http-errors');
const express = require('express');
const validator = require('express-validator');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const mongodbUri = require('mongodb-uri');
const passport = require('passport');
const session = require('express-session');

const hbs = require("hbs");
hbs.registerHelper("equal", require("handlebars-helper-equal"));

require('./passport');
const config = require('./config');

// API - Files
const fw_api = require('./routes/api');

// VIEWS FILES
const adminRegister = require('./controllers/Admin/Authentication/auth');
const addFieldOfficer = require('./controllers/Admin/FieldOfficers/fieldOfficers');
const addCoordinator = require('./controllers/Admin/ProjectCoordinator/coordinator');
const addProject = require('./controllers/Admin/Projects/projects');
const createTeam = require('./controllers/Admin/Teaming/team');
const createTask = require('./controllers/Admin/Tasking/tasks');
const surveyData = require('./controllers/Admin/Surveys/surveys');

mongoose.Promise = global.Promise;
let mongooseConnectString;
mongooseConnectString = mongodbUri.formatMongoose(config.livedbconnection);
mongoose.connect(mongooseConnectString,{
	useNewUrlParser: true,
	reconnectInterval: Number.MAX_VALUE,
 });

// Test for connection success
mongoose.set('useCreateIndex', true);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error: '));
db.once('open', function callback () {
    console.log('Successfully connected to MongoDB');
});

global.User = require('./models/admin/user');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use('/uploads',express.static('uploads'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(validator());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session({
	cookie: {
		maxAge : 3600000,
		// expires: true | Date.now() + 90000
	},
  secret: config.sessionKey,
  resave: false,
  saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  if (req.isAuthenticated()) {
    res.locals.user = req.user;
  }
  next();
});

// API - ROUTES
app.use('/api/v1/worker', fw_api);

// VIEWS - ROUTES
app.use('/', adminRegister);
app.use('/', addFieldOfficer);
app.use('/', addCoordinator);
app.use('/', addProject);
app.use('/', createTeam);
app.use('/', createTask);
app.use('/', surveyData);

(function runForever(){
	console.log("Welcome PM: 🐙 server Running");
	setTimeout(runForever, 1000);
  })();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// start server and listen on port
const server = app.listen(process.env.PORT || 3004, () => {
    const port = server.address().port;
    console.log('App is now running on port', port);
});

// module.exports = app;
