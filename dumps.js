<div class="grid-container col-12">
<div class="grid-item">1</div>
<div class="grid-item">1</div>
<div class="grid-item">1</div>
</div>
<!-- Card -->
<div>
	<div class="card" id="item">
		<div class="card-body">

			<div class="card">

				<img src="/img/covers/profile-cover-2.jpg" alt="..." class="card-img-top">

				<div class="card-body text-center">

					<h2 class="card-title">
						<a href="profile-posts.html">{{propertyName}}</a>
					</h2>

					<p class="card-text text-muted">
						<small>
							{{propertyDesc}}
						</small>
					</p>

					<p class="card-text">
						<span class="badge badge-soft-secondary">
							{{propertyLevel}}
						</span>
						<span class="badge badge-soft-secondary">
							{{propertyPrice}} per Month
						</span>
					</p>

					<hr>

					<div class="row align-items-center justify-content-between">
						<div class="col-auto">

							<small>
								<span class="text-success">●</span> Online
							</small>

						</div>
						<div class="col-auto">

							<a href="components.html#!" class="btn btn-sm btn-primary">
								View More
							</a>

						</div>
					</div>

				</div>

			</div>

		</div>
	</div>
</div>
