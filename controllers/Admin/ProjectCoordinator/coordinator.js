const { ProjectCoordinators } = require('../../../models/ProjectCoordinators/coordinators');
const express = require('express');
const router = express.Router();

router.route('/addcoordinator')
	.get((req,res,next) => {
		if (req.user != null || req.user != undefined){
			ProjectCoordinators.find({

			},(err, data) => {
				if (err){
					console.log(err.message);
				}else{
					res.render('ProjectCoordinator/addcoordinator',{
						layout: 'Admin/dashboard_layout',
						title: 'PM - Add Coordinator',
						allData: data,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})
	.post((req,res,next) => {
	const name = req.body.name;
	const mobileNumber = req.body.mobileNumber;
	const gender = req.body.gender;
	const age = req.body.age;
	const location = req.body.location;

	ProjectCoordinators.findOne({
		'mobile': mobileNumber
	},(err,found) => {
		if (err) {
			res.status(400).json({
				successful: false,
				message: err.message
			});
		} else {
			if (!found) {
				// new coordinator
				const coordinator = new ProjectCoordinators({
					coordinatorName: name,
					mobile: mobileNumber,
					gender: gender,
					age: age,
					location: location
				});
				coordinator.save((err,saved) => {
					if (err) {

						ProjectCoordinators.find({

						},(error, data) => {
							if (error){
								console.log(error.message);
							}else{
								console.log(data)

								res.render('ProjectCoordinator/addcoordinator', {
									layout: 'Admin/dashboard_layout',
									errorMesage: err.message,
									allData: data
								});
							}
						});
					} else {

						ProjectCoordinators.find({

						},(error, data) => {
							if (error){
								console.log(error.message);
							}else{
								console.log(data)
								res.render('ProjectCoordinator/addcoordinator', {
									layout: 'Admin/dashboard_layout',
									message: saved,
									allData: data
								});
							}
						});
					}
				});
			} else {

				ProjectCoordinators.find({

				},(error, data) => {
					if (error){
						console.log(error.message);
					}else{
						console.log(data)
						res.render('ProjectCoordinator/addcoordinator', {
							layout: 'Admin/dashboard_layout',
							failed: `${name} already created`,
							data: found,
							allData: data
						});
					}
				});
				return;
			}
		}
	});
})
.patch((req,res) => {
	const coordinatorId = req.body.coordinatorId;

	const coordinatorName = req.body.coordinatorName;
	const coordinatorMobile = req.body.coordinatorMobile;
	const coordinatorGender = req.body.coordinatorGender;
	const coordinatorAge = req.body.coordinatorAge;
	const coordinatorLocation = req.body.coordinatorLocation;

	ProjectCoordinators.findOneAndUpdate({
		'_id': coordinatorId
	},{
		$set: {
			'coordinatorName': coordinatorName,
			'mobile': coordinatorMobile,
			'gender': coordinatorGender,
			'age': coordinatorAge,
			'location': coordinatorLocation
		}
	},{
		new: true
	},(err,updated) => {
		if (err){
			console.log(err.message);
			res.end();
		}else{
			console.log(updated);
			res.end();
		}
	})
})
.delete((req,res) => {

	const coordinatorId = req.body.coordinatorId;

	console.log("here: " + coordinatorId);

	ProjectCoordinators.findOneAndDelete({
		'_id': coordinatorId
	},(err, results) => {
		if (err){
			console.log(err)
		}else{
			ProjectCoordinators.find({

			},(error, data) => {
				if (error){
					console.log(error.message);
				}else{
					console.log(data)
					res.render('ProjectCoordinator/addcoordinator', {
						layout: 'Admin/dashboard_layout',
						message: results,
						allData: data
					});
				}
			});
		}
	})
});

router.route('/coordinators')
	.get((req,res,next) => {
		if (req.user != null || req.user != undefined){
			ProjectCoordinators.find({

			},(err, coordinators) => {
				if (err){
					console.log(err.message);
				}else{

					const sorted = coordinators.reverse()
					res.render('ProjectCoordinator/allcordinators', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Field coordinators',
						data: sorted,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})

module.exports = router;
