const { TaskModel } = require('../../../models/Tasking/task');
const { ProjectCoordinators } = require('../../../models/ProjectCoordinators/coordinators');
const { ProjectModel } = require('../../../models/Projects/projects');
const express = require('express');
const router = express.Router();

router.route('/createtask')
	.get((req,res,next) => {
		TaskModel.find({

		},(error,tasks) => {
			if (error) {
				throw new Exception(err.message);
			} else {
				ProjectCoordinators.find({

				},(err,coordinators) => {
					if (err) {
						throw new Exception(err.message);
					} else {

						ProjectModel.find({

						},(err,projs) => {
							if (err) {
								console.log(err.message);
							} else {
								res.render('Tasking/createtasks', {
									layout: 'Admin/dashboard_layout',
									title: 'Admin - Create Tasks',
									pcs: coordinators,
									tasks: tasks,
									userData: req.user,
									projects: projs
								});
							}
						})
					}
				})
			}
		})
	})
	.post((req,res,next) => {

		const taskName = req.body.taskName;
		const taksDescription = req.body.taskDescription;
		const pcoordinator = req.body.coordinator;
		const startDate = req.body.startDate;
		const endDate = req.body.endDate;
		const projectsId = req.body.projectsId;

		console.log(req.body);

		ProjectCoordinators.findOne({
			'mobile': pcoordinator
		},(err,foundpcs) => {
			if (err) {
				console.log(err.message);
			} else {

				ProjectModel.findOne({
					'_id': projectsId
				},(err,projs) => {
					if (err) {
						console.log(err.message);
					} else {
						const task = new TaskModel({
							taskName: taskName,
							taskDescription: taksDescription,
							startDate: startDate,
							endDate: endDate,
							projectCoredinator: foundpcs,
							project: projs
						});

						task.save((error, savedTask) => {
							if (error) {
								console.log(error.message);
							} else {
								res.redirect('/createtask');
							}
						})
					}
				})
			}
		})
	})


// all tasks
router.route('/alltasks')
	.get((req,res,next) => {

		TaskModel.find({

		},(err,tasks) => {
			if (err) {
				console.log(err.message);
			} else {
				res.render('Tasking/alltasks', {
					layout: 'Admin/dashboard_layout',
					title: 'Admin - All Tasks',
					tasks: tasks,
					userData: req.user
				});
			}
		})
	})

module.exports = router;
