const { ProjectModel } = require('../../../models/Projects/projects');
const express = require('express');
const router = express.Router();

router.route('/addprojects')
	.get((req,res) => {
		if (req.user != null){
			ProjectModel.find({

			},(err, data) => {
				if (err){
					console.log(err.message);
				}else{
					res.render('Projects/addprojects',{
						layout: 'Admin/dashboard_layout',
						title: 'PM - Add Project',
						allData: data,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})
	.post((req,res) => {
	const name = `${req.body.name}`;
	const desc = req.body.description;
	const duration = req.body.duration;
	const location = req.body.location;

	ProjectModel.findOne({
		'projectName': name
	},(err,found) => {
		if (err) {
			res.status(400).json({
				successful: false,
				message: err.message
			});
		} else {
			if (!found) {
				// new Project
				const project = new ProjectModel({
					projectName: name,
					ProjectDescription: desc,
					projectDuration: duration,
					location: location
				});
				project.save((err,saved) => {
					if (err) {

						ProjectModel.find({

						},(error, data) => {
							if (error){
								console.log(error.message);
							}else{
								console.log(data);

								res.render('Projects/addprojects', {
									layout: 'Admin/dashboard_layout',
									errorMessage: err.message,
									allData: data
								});
							}
						});
					} else {

						ProjectModel.find({

						},(error, data) => {
							if (error){
								console.log(error.message);
							}else{
								console.log(data);
								res.render('Projects/addprojects', {
									layout: 'Admin/dashboard_layout',
									message: saved,
									allData: data
								});
							}
						});
					}
				});
			} else {

				ProjectModel.find({

				},(error, data) => {
					if (error){
						console.log(error.message);
					}else{
						console.log(data);
						res.render('Projects/addprojects', {
							layout: 'Admin/dashboard_layout',
							failed: `${name} already created`,
							data: found,
							allData: data
						});
					}
				});
			}
		}
	});
})
.patch((req,res) => {

	const projectId = req.body.projectId;
	const projectName = req.body.projectName;
	const projectDescription = req.body.projectDescription;
	const projectDuration = req.body.projectDuration;
	const location = req.body.location;

	ProjectModel.findOneAndUpdate({
		'_id': projectId
	},{
		$set: {
			'projectName': projectName,
			'projectDescription': projectDescription,
			'projectDuration': projectDuration,
			'location': location
		}
	},{
		new: true
	},(err,updated) => {
		if (err){
			console.log(err.message);
			res.end();
		}else{
			console.log(updated);
			res.end();
		}
	})
})
.delete((req,res) => {

	const projectId = req.body.projectId;

	console.log("here: " + projectId);

	ProjectModel.findOneAndDelete({
		'_id': projectId
	},(err, results) => {
		if (err){
			console.log(err)
		}else{
			ProjectModel.find({

			},(error, data) => {
				if (error){
					console.log(error.message);
				}else{
					console.log(data);
					res.render('Projects/addprojects', {
						layout: 'Admin/dashboard_layout',
						message: results,
						allData: data
					});
				}
			});
		}
	})
});

router.route('/projects')
	.get((req,res) => {
		if (req.user != null){
			ProjectModel.find({

			},(err, Projects) => {
				if (err){
					console.log(err.message);
				}else{

					const sorted = Projects.reverse();

					console.log(Projects);

					res.render('Projects/allprojects', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Projects',
						data: sorted,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	});

	// ongoing projects
router.route('/ongoingprojects')
	.get((req,res) => {
		if (req.user != null){
			ProjectModel.find({
				'projectState': 2
			},(err, Projects) => {
				if (err){
					console.log(err.message);
				}else{

					const sorted = Projects.reverse();
					res.render('Projects/ongoingprojects', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Projects',
						data: sorted,
						isActive: true,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	});

// past projects
router.route('/pastprojects')
	.get((req,res) => {
		if (req.user != null){
			ProjectModel.find({
				'projectState': 3
			},(err, Projects) => {
				if (err){
					console.log(err.message);
				}else{

					const sorted = Projects.reverse();
					res.render('Projects/pastprojects', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Projects',
						data: sorted,
						isActive: false,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	});

router.route('/projectdetail')
	.post((req,res) => {

	const projectId = req.body.projectId;

	ProjectModel.findOne({
		'_id': projectId
	},(err,project) => {
		if (err) throw err.message;
			const propertyName = `${property.numberOfRooms},${property.type},${property.city}`;
			res.render('Admin/property_detail', {
				layout: 'Admin/dashboard_layout',
				title: propertyName,
				data: project,
				userData: req.user
			});
		})
	});

module.exports = router;
