const express = require('express')
const router = express.Router();

const { SchemaModel } = require('../../../models/Survey/surveys');

router.route('/survey')
	.get((req,res) => {
		res.render('surveys/surveys', {
			layout: 'Admin/dashboard_layout',
			title: 'Admin - All Surveys',
			userData: req.user
		});
	})
	.post((req,res,next) => {

		// incoming form  data
		const mobile = req.body.mobileNumber;

	});

router.route('/surveydata')
	.get((req,res) => {
		res.render('surveys/surveydata', {
			layout: 'Admin/dashboard_layout',
			title: 'Admin - All Surveys',
			userData: req.user
		});
	});

module.exports = router;
