const express = require('express');
const router = express.Router();
const { TeamModel } = require('../../../models/Teaming/teams');
const { TaskModel } = require('../../../models/Tasking/task');
const { FieldOfficers } = require('../../../models/FieldOfficers/fieldofficers');
const { ProjectModel } = require('../../../models/Projects/projects');

router.route('/createteam')
	.get((req,res,next) => {
		// show all field workers
		ProjectModel.find({

		},(error,projects) => {
			if (error) {
				throw new Exception(err.message);
			} else {
				FieldOfficers.find({

				},(err,officers) => {
					if (err) {
						throw new Exception(err.message);
					} else {

						TaskModel.find({

						},(terr,results) => {
							if (terr) {
								throw new Exception(terr.message);
							} else {
								TeamModel.find({

								},(newerr,newdata) => {
									console.log(officers);
									res.render('Teaming/createteam', {
										layout: 'Admin/dashboard_layout',
										title: 'Admin - Create Team',
										workers: officers,
										projects: projects,
										userData: req.user,
										tasks: results,
										data: newdata
									});
								})
							}
						})
					}
				})
			}
		})
	})
	.post((req,res) => {

		const teamName = req.body.teamName;
		const teamPurpose = req.body.teamPurpose;
		const teamMembers = req.body.teamMembers;
		// const teamProject = req.body.teamProject;
		const teamTask = req.body.teamTask;
		const createdBy = req.body.createdBy;

		console.log(req.body);

		var memberData = [];
		if (typeof teamMembers === "string"){
			memberData.push(teamMembers);
		} else{
			teamMembers.forEach(element => {
				memberData.push(element);
			});
		}

		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();

		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}

		today = dd + '/' + mm + '/' + yyyy; //mm + '/' + dd + '/' + yyyy;
		const dateCreated = today;

		TaskModel.findOne({
			'_id': teamTask
		},(err,tasks) => {
			if (err) {
				console.log(err.message);
			} else {
				const team = new TeamModel({
					teamName: teamName,
					teamPurpose: teamPurpose,
					teamMembers: memberData,
					// teamProject: teamProject,
					teamTask: tasks,
					createdBy: createdBy,
					dateCreated: dateCreated
				});

				team.save((err, savedTeam) => {
					if (err) {
						console.log(err.message);
					} else {
						res.redirect('/createteam');
					}
				})
			}
		})
	});


	// all team members
router.route('/allteams')
	.get((req,res) => {

		TeamModel.find({

		},(err,teams) => {
			if (err) {
				throw new Exception(err.message)
			} else {
				console.log(teams);
				res.render('Teaming/allteams', {
					layout: 'Admin/dashboard_layout',
					title: 'Admin - All Teams',
					data: teams,
					userData: req.user
				});
			}
		})
	});

module.exports = router;
