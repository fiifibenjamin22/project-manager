const express = require('express')
const router = express.Router();

router.route('/dashboard')
	.get((req,res,next) => {
		res.render('Admin/dashboard', {layout: 'Admin/dashboard_layout',title: 'Admin-home'});
	})
	.post((req,res,next) => {

	});

module.exports = router;
