const express = require('express')
const { LevelModel } = require('../../../models/property/level');
const { Property } = require('../../../models/property/property');
const userModel = require('../../../models/admin/user');
const router = express.Router();

router.route('/myproperty')
	.get((req,res,next) => {
		console.log('user data: ')
		console.log(req.user);

		if (req.user != null || req.user != undefined){

			if (req.user.isAdmin == true) {
				Property.find({

				},(err,myproperties) => {
					if (err){
						res.status(400).json({
							successfull: false,
							message: err.message
						})
					}else{
						//console.log(myproperties);
						const sorted = myproperties.reverse()
						res.render('Admin/myproperty', {
							layout: 'Admin/dashboard_layout',
							title: 'Admin - My Properties',
							data: sorted,
							userData: req.user
						});
					}
				})
			} else {
				const userId = req.user._id;
				Property.find({
					'userId': userId
				},(err, myproperties) => {
					if (err){
						res.status(400).json({
							successfull: false,
							message: err.message
						})
					}else{
						console.log(myproperties);
						const sorted = myproperties.reverse()
						res.render('Admin/myproperty', {
							layout: 'Admin/dashboard_layout',
							title: 'Admin - My Properties',
							data: sorted,
							userData: req.user
						});
					}
				})
			}
		}else{
			res.redirect('/login');
		}
	})
	.post((req,res,next) => {
		const id = req.body.propId;
		console.log(id);
		Property.findOneAndDelete({
			'_id': id
		},(err,deleted) => {
			if (err) {
				res.status(400).json({
					successfull: false,
					message: err.message
				})
			} else {
				console.log(deleted);
				res.redirect('/myproperty');
			}
		})
	});

// property detail
router.route('/propertydetail')
	.post((req,res,next) => {
		const propId = req.body.propertyId;
		console.log("hello: " + propId);
		Property.findOne({
			'_id': propId
		},(err,property) => {
			if (err){
				res.status(400).json({
					successfull: false,
					message: err.message
				});
			}else{
				console.log(property);
				const propertyName = `${property.numberOfRooms},${property.type},${property.city}`;
				res.render('Admin/property_detail', {
					layout: 'Admin/dashboard_layout',
					title: propertyName,
					data: property,
					userData: req.user
				});
			}
		});
	});

module.exports = router;
