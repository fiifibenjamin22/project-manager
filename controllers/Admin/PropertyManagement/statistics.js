const express = require('express')
const router = express.Router();

router.route('/statistics')
	.get((req,res,next) => {
		res.render('Admin/statistics', {layout: 'Admin/dashboard_layout',title: 'Admin - Statistics'})
	})
	.post((req,res,next) => {

	});

module.exports = router;
