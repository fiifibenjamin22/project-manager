const express = require('express');
const multer = require('multer');

const { LevelModel } = require('../../../models/property/level');
const { Property } = require('../../../models/property/property');
const router = express.Router();

const storage = multer.diskStorage({
	filename: (req, file, cb) => {
		cb(null, new Date().toISOString() + file.originalname);
	}
});

const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
		cb(null, true);
	}else{
		cb(null, false);
	}
};

const upload = multer({
	storage: storage,
	fileFilter: fileFilter
});


router.route('/addproperty')
	.get((req,res) => {

		if (req.user != null || req.user != undefined){
			LevelModel.find({

			},(err,results) => {
				if (err){
					console.log(err.message);
				}else{
					res.render('Admin/addproperty', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Add Property',
						levelData: results,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})
	.post(upload.array('photos',5),(req,res,next) => {

		let imageList = [];
		for (const i in req.files) {
			if (req.files.hasOwnProperty(i)) {
				const element = req.files[i];
				console.log(element.path);
				imageList.push('https://direct-city-homes.herokuapp.com/' + element.path);
			}
		}

		// body data
		const posterId = req.body.posterId;

		const llName = req.body.landlordname;
		const llMobile = req.body.landlordmobilenumber;

		console.log("print: " + llName);
		console.log("print: " + llMobile);

		var pType;

		if (req.body.propertyType == "0") {
			return
		} else {
			pType = req.body.propertyType;
		}
		const pprice = req.body.pprice;
		const pNumberOfRooms = req.body.numberOfRooms;
		const pNumberOfWahsrooms = req.body.numberOfWashrooms;
		const pHasGuarrage = req.body.hasguarrage;

		const pdesc = req.body.pdesc;
		const pcountry = req.body.pcountry;
		const pregion = req.body.pregion;
		const pcity = req.body.pcity;

		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();

		if (dd < 10) {
			dd = '0' + dd;
		}

		if (mm < 10) {
			mm = '0' + mm;
		}

		today = dd + '/' + mm + '/' + yyyy; //mm + '/' + dd + '/' + yyyy;
		const dateAdded = today;

		const longlat = req.body.coordinates;

		const lName = req.body.levelName;
		console.log(lName);

		LevelModel.findOne({
			'levelName': lName
		},(err, result) => {
			if(err){
				res.status(400).json({
					message: err.message
				});
			}else{
				if (result == null) {
					res.status(404).json({
						message: 'level rate not found'
					});
				}else{
					console.log(result.levelRate);

					const newProperty = new Property({
						userId: posterId,
						landlordName: llName,
						landlordContact: llMobile,

						propertyType: pType,
						numberOfRooms: pNumberOfRooms,
						numberOfWashrooms: pNumberOfWahsrooms,
						carPort: pHasGuarrage,
						propertyLevel: req.body.levelName,
						propertyLevelRate: result.levelRate,
						propertyPrice: pprice,
						propertyDesc: pdesc,
						propertyImages: imageList,
						dateAdded: dateAdded,

						locationFromMap: longlat,
						country: pcountry,
						region: pregion,
						city: pcity
					});

					newProperty.save((err, results) => {
						if (err){
							console.log(err.message);
							res.render('Admin/addproperty',
							{
								layout: 'Admin/dashboard_layout',
								title: 'Admin - Add Property',
								errorMessage: err.message
							});
						}else{

							console.log(results);

							res.render('Admin/myproperty',
							{
								layout: 'Admin/dashboard_layout',
								title: 'Admin - My Property',
								propertyData: results,
								userData: req.user
							});
						}
					});
				}
			}
		});
	});

router.post('/find/level',(req,res) => {

	LevelModel.findOne({
		'levelName': req.body.levelName
	},(err, results) => {
		if(err) throw err.message;
		console.log(results.levelRate);
		res.end();
	});
});

module.exports = router;
