const express = require('express');
const { LevelModel } = require('../../../models/property/level');
const router = express.Router();

router.route('/levels')
	.get((req,res) => {

		if (req.user != null || req.user != undefined){
			LevelModel.find({

			},(err, data) => {
				if (err){
					console.log(err.message);
				}else{
					res.render('Admin/levelmanager',{
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Level Manager',
						allData: data,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})
	.post((req, res) => {

		const levelName = req.body.levelName;
		const levelRate = req.body.levelRate;

		LevelModel.findOne({
			'levelName': levelName
		},(err, level) => {
			if (err){
				res.status(400).json({
					successful: false,
					message: err.message
				});
			}else{
				if (level){

					console.log("already exist"+level);

					LevelModel.find({

					},(error, data) => {
						if (error){
							console.log(error.message);
						}else{
							console.log(data)
							res.render('Admin/levelmanager', {
								layout: 'Admin/dashboard_layout',
								failed: "Level Already Created",
								data: level,
								allData: data
							});
						}
					});
					return;

				}else{

					const newLevel = new LevelModel({
						levelName: levelName,
						levelRate: levelRate
					})

					newLevel.save((err,results) => {
						if (err){

							LevelModel.find({

							},(error, data) => {
								if (error){
									console.log(error.message);
								}else{
									console.log(data)

									res.render('Admin/levelmanager', {
										layout: 'Admin/dashboard_layout',
										errorMesage: err.message,
										allData: data
									});
								}
							});
						}else{
							//console.log(results);
							LevelModel.find({

							},(error, data) => {
								if (error){
									console.log(error.message);
								}else{
									console.log(data)
									res.render('Admin/levelmanager', {
										layout: 'Admin/dashboard_layout',
										message: results,
										allData: data
									});
								}
							});
						}
					});
				}
			}
		})
	})
	.delete((req,res) => {

		const levelId = req.body.levelId;

		console.log("here: " + levelId);

		LevelModel.findOneAndDelete({
			'_id': levelId
		},(err, results) => {
			if (err){
				console.log(err)
			}else{
				LevelModel.find({

				},(error, data) => {
					if (error){
						console.log(error.message);
					}else{
						console.log(data)
						res.render('Admin/levelmanager', {
							layout: 'Admin/dashboard_layout',
							message: results,
							allData: data
						});
					}
				});
			}
		})
	})
	.patch((req,res) => {
		const levelId = req.body.levelId;

		const levelName = req.body.levelName;
		const levelRate = req.body.levelRate;

		console.log(levelName);
		console.log(levelRate);
		console.log(levelId);

		LevelModel.findOneAndUpdate({
			'_id': levelId
		},{
			$set: {
				'levelName': levelName,
				'levelRate': levelRate
			}
		},{
			new: true
		},(err,updated) => {
			if (err){
				console.log(err.message);
				res.end();
			}else{
				console.log(updated);
				res.end();
			}
		})
	});

	module.exports = router;
