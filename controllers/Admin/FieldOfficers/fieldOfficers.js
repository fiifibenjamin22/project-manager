const { FieldOfficers } = require('../../../models/FieldOfficers/fieldofficers');
const user = require('../../../models/admin/user');
const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();
const SALT_I = 10;

router.route('/addfieldofficer')
	.get((req,res) => {
		if (req.user != null || req.user != undefined){

			user.find({
				'type': 'Field Worker'
			},(err, workers) => {
				if (err) throw err.message;
				FieldOfficers.find({

				},(err, data) => {
					if (err){
						console.log(err.message);
					}else{
						console.log(workers);
						res.render('FieldOfficers/addfieldOfficers',{
							layout: 'Admin/dashboard_layout',
							title: 'PM - Add Officer',
							allData: data,
							workerData: workers,
							userData: req.user
						});
					}
				});
			});
		}else{
			res.redirect('/login');
		}
	})
	.post((req,res) => {

		const mobileNumber = req.body.mobileNumber;

		FieldOfficers.findOne({
			'mobile': mobileNumber
		},(err,found) => {
			if (err) {
				res.status(400).json({
					successful: false,
					message: err.message
				});
			} else {
				if (!found) {

					user.findOne({
						'mobileNumber': mobileNumber
					},(err, foundWorker) => {
						console.log(foundWorker);

						if (err) throw err.message;

						const officer = new FieldOfficers({
							officerName: `${foundWorker.firstName} ${foundWorker.lastName}`,
							mobile: `${foundWorker.mobileNumber}`,
							gender: foundWorker.gender,
							age: foundWorker.age,
							location: foundWorker.location
						});

						officer.save((err, worker) =>{
							if (err) throw err.message;

							FieldOfficers.find({

							},(error, data) => {
								if (error){
									console.log(error.message);
								}else{
									console.log(data);
									res.render('FieldOfficers/addfieldOfficers', {
										layout: 'Admin/dashboard_layout',
										data: found,
										workers: worker,
										allData: data
									});
								}
							});
						});
					});
				} else {

					FieldOfficers.find({

					},(error, data) => {
						if (error){
							console.log(error.message);
						}else{
							console.log(data);
							res.render('FieldOfficers/addfieldOfficers', {
								layout: 'Admin/dashboard_layout',
								failed: `${found.firstName} already created`,
								data: found,
								allData: data
							});
						}
					});
				}
			}
		});
	})
	.patch((req,res) => {
	const officerId = req.body.officerId;

	const officerName = req.body.officerName;
	const officerMobile = req.body.officerMobile;
	const officerGender = req.body.officerGender;
	const officerAge = req.body.officerAge;
	const officerLocation = req.body.officerLocation;

	FieldOfficers.findOneAndUpdate({
		'_id': officerId
	},{
		$set: {
			'officerName': officerName,
			'mobile': officerMobile,
			'gender': officerGender,
			'age': officerAge,
			'location': officerLocation
		}
	},{
		new: true
	},(err,updated) => {
		if (err){
			console.log(err.message);
			res.end();
		}else{
			console.log(updated);
			res.end();
		}
	});
})
.delete((req,res) => {

	const officerId = req.body.officerId;

	console.log("here: " + officerId);

	FieldOfficers.findOneAndDelete({
		'_id': officerId
	},(err, results) => {
		if (err){
			console.log(err)
		}else{
			FieldOfficers.find({

			},(error, data) => {
				if (error){
					console.log(error.message);
				}else{
					console.log(data)
					res.render('Admin/levelmanager', {
						layout: 'Admin/dashboard_layout',
						message: results,
						allData: data
					});
				}
			});
		}
	})
});

router.route('/fieldofficers')
	.get((req,res,next) => {
		if (req.user != null || req.user != undefined){
			FieldOfficers.find({

			},(err, officers) => {
				if (err){
					console.log(err.message);
				}else{

					const sorted = officers.reverse()
					res.render('FieldOfficers/allfieldofficers', {
						layout: 'Admin/dashboard_layout',
						title: 'Admin - Field Officers',
						data: sorted,
						userData: req.user
					});
				}
			});
		}else{
			res.redirect('/login');
		}
	})

module.exports = router;
