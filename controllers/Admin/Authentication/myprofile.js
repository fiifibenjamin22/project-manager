const express = require('express')
const router = express.Router();

router.route('/profile')
	.get((req,res,next) => {
		if (req.user != null || req.user != undefined) {
			res.render('Admin/myprofile',{
				layout: 'Admin/dashboard_layout',
				title: 'My Profile',
				userData: req.user
			})
		}else{
			res.redirect('/login');
		}
	})
	.post((req,res,next) => {

	});

module.exports = router;
