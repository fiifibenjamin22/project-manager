const express = require('express')
const router = express.Router();
const passport = require('passport');
// const { Property } = require('../../../models/property/property');

var sessionChecker = (req, res, next) => {
	console.log(req.session.user);
	if (req.session.user && req.cookies.user_sid) {
		// res.redirect('/dashboard');
		res.render('Admin/dashboard', {layout: 'Admin/dashboard_layout',title: 'PM - Home'});
	} else {
		next();
	}
};

router.route('/register')
	.get((req, res, next) => {
		res.render('Admin/register', {layout: 'Admin/auth_layout',title: 'PM - Admin-Register', description: 'PM - New Account'});
	})
	.post((req, res, next) => {
		req.checkBody('firstname', 'Empty First Name').notEmpty();
		req.checkBody('lastname', 'Empty Last Name').notEmpty();
		// req.checkBody('email', 'Invalid Email').isEmail();
		// req.checkBody('mobilenumber','Incorrect Mobile Number').isMobilePhone();
		// req.checkBody('password', 'Empty Password').notEmpty();
		// req.checkBody('confirmpassword', 'Password do not match').equals(req.body.confirmPassword).notEmpty();

		var errors = req.validationErrors();
        if (errors) {
			console.log(errors);
            res.render('Admin/register', {
                lastName: req.body.lastname,
                email: req.body.email,
                errorMesage: errors
            });
        }else {
			var user = new User();
			if (req.body.type !== 'Property Owner' || req.body.type !== 'Content Provider') {
				user.isAdmin = false;
			} else {
				user.isAdmin = true;
			}
			user.firstName = req.body.firstname;
			user.lastName = req.body.lastname;
			user.mobileNumber = req.body.mobilenumber;
			user.type = req.body.type;
			user.email = req.body.email;
			user.gender = req.body.gender;
			user.age = req.body.age;
			user.location = req.body.location;
            user.setPassword(req.body.password);
            user.save((err,user) => {
                if (err) {
					console.log(err.message);
					res.render('Admin/register', {
						layout: 'Admin/auth_layout',
                        errorMesage: err
                    })
                }else{
                    res.redirect('/login');
                }
            })
        }
	});

router.route('/login')
	.get((req,res,next) => {
		res.render('Admin/login', {
			layout: 'Admin/auth_layout',
			title: 'PM - Admin-Login',
			description: 'Please Sign In'
		});
	})
	.post(passport.authenticate('local-login', {
		failureRedirect: '/login'
	}), (req, res) => {
		res.redirect('/');
	});


router.route('/')
	.get((req,res) => {
		console.log(req.user);
		if (req.user != null || req.user !== undefined){
			if (req.user.type == "Administrator") {
				res.render('Admin/dashboard',{
					layout: 'Admin/dashboard_layout',
					title: 'PM - Dashboard',
					description: 'Admin Home Dashboard',
					userData: req.user
				});
			} else if (req.user.type == "Project Manager"){
				res.render('Admin/dashboard',{
					layout: 'ProjectCoordinator/pc_layout',
					title: 'PM - Dashboard',
					description: 'Admin Home Dashboard',
					userData: req.user
				});
			} else {
				res.render('Admin/dashboard',{
					layout: 'FieldOfficers/fw_layout',
					title: 'PM - Dashboard',
					description: 'Admin Home Dashboard',
					userData: req.user
				});
			}
		}else{
			res.redirect('/login');
		}
	})

module.exports = router;
