const { SchemaModel } = require('./../../../models/Survey/surveys');

exports.addsurveyData = (req, res, next) => {

	const sDate = req.body.sDate;
	const vslaName = req.body.vslaName;
	const vslaLocation = req.body.vslaLocation;
	const FacilitatorName = req.body.FacilitatorName;
	const memberName = req.body.memberName;

	const survey = new SchemaModel({

		sDate: sDate,
		vslaName: vslaName,
		vslaLocation: vslaLocation,
		FacilitatorName: FacilitatorName,
		memberName: memberName
	});

	survey.save((err, data) => {
		if (err) {
			res.status(400).json({
				sucessful: false,
				message: err.message
			})
		} else {
			res.status(200).json({
				sucessful: true,
				message: data
			});
		}
	})

}

exports.attendance = ((req, res, next) => {

	const attendanceResponse = req.body.attendanceResponse;

	SchemaModel.findOneAndUpdate({
		'_id': req.body.memberId
	},{
		$set: {
			'memberPresent': attendanceResponse
		}
	},{
		new: true
	},(err, updated) => {
		if (err) {
			res.status(400).json({
				sucessful: false,
				message: err.message
			})
		} else {
			res.status(200).json({
				sucessful: true,
				message: updated
			})
		}
	})
})
