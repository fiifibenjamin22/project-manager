const bcrypt = require('bcryptjs');
const { FieldOfficers } = require('./../../../models/FieldOfficers/fieldofficers');

exports.login = (req, res, next) => {

	const mobileNumber = req.body.mobileNumber;
	const password = req.body.password;

	FieldOfficers.findOne({
		'mobile': mobileNumber
	},(err,fw) => {
		if (err) {
			res.status(400).json({
				sucessful: false,
				message: err.message
			})
		} else {

			if (!fw) {
				res.status(400).json({
					message: 'You have not registered, kindly register'
				})
			} else {

				bcrypt.compare(password, fw.password, (err, isMatch) => {
					if (err) {
						return next(err);
					} else {
						if (isMatch === false) {
							res.status(400).json({
								message: 'Password is not correct, please type again'
							})
						} else {
							fw.generateToken((err, workers) => {
								if (err) {
									res.status(400).json({
										message: err.message
									});
								} else {
									res.cookie('auth', workers.token).json({
										message: workers
									});
								}
							})
						}
					}
				})
			}
		}
	})
}
