'use strict'

module.exports = {
    mailer: {
        service: 'Gmail',
        auth: {
            user: 'email goes here',
            pass: 'password to email goes here'
        }
    },
	dbConnectString: 'mongodb://localhost/project_manager',
	livedbconnection: 'mongodb://octopus:octopus22@ds251362.mlab.com:51362/project_manager',
    sessionKey: 'projectmanager'
}
