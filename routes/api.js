const express = require('express');
const router = express.Router();

const authentications = require('../controllers/API/Authentications/authentications');
const attendance = require('../controllers/API/Survey/surveys');

router.post('/login', authentications.login);
router.post('/member/add', attendance.addsurveyData);
router.patch('/member/update', attendance.attendance);

module.exports = router;
