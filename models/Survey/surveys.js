const mongoose = require('mongoose')

const surveySchema = mongoose.Schema({

	sDate: {
		type: String,
		required: true
	},

	vslaName: {
		type: String,
		required: true
	},

	vslaLocation: {
		type: String,
		required: true
	},

	FacilitatorName: {
		type: String,
		required: true
	},

	memberName: {
		type: String,
		required: true
	},

	memberPresent: {
		type: Boolean,
		default: false
	}

});

const SchemaModel = mongoose.model('Surveys', surveySchema);
module.exports = { SchemaModel };
