const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({

	taskName: {
		type: String,
		required: true
	},

	taskDescription: {
		type: String,
		required: true
	},

	startDate: {
		type: String,
		required: true
	},

	endDate: {
		type: String,
		required: true
	},

	project:{
		type: Object,
		required: true
	},

	projectCoredinator:{
		type: Object,
		required: true
	}
});

const TaskModel = mongoose.model('Tasks', taskSchema);
module.exports = { TaskModel };
