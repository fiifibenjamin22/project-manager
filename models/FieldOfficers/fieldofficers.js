const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const fieldOfficerSchema = mongoose.Schema({

	officerName: {
		type: String,
		required: true
	},

	mobile: {
		type: String,
		required: true
	},

	gender: {
		type: String,
		required: true
	},

	age: {
		type: String,
		required: true
	},

	location: {
		type: String,
		required: true
	}
});

const FieldOfficers = mongoose.model('Field Officers', fieldOfficerSchema);
module.exports = { FieldOfficers };
