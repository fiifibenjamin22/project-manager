const mongoose = require('mongoose');

const projectSchema = mongoose.Schema({

	projectName: {
		type: String,
		required: true
	},

	ProjectDescription: {
		type: String,
		required: true
	},

	projectDuration: {
		type: String,
		required: true
	},

	location: {
		type: String,
		required: true
	},

	projectState: {
		type: Number,
		max: 3,
		min: 1,
		default: 1
	}

});

const ProjectModel = mongoose.model('Projects', projectSchema);
module.exports = { ProjectModel };
