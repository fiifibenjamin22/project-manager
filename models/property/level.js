const mongoose = require('mongoose');

const levelSchema = mongoose.Schema({

	levelName: {
		type: String,
		required: true
	},

	levelRate: {
		type: String,
		required: true
	}
});

levelSchema.index({
	levelName : 'text',
	levelRate : 'text'
});

const LevelModel = mongoose.model('Level', levelSchema);
module.exports = { LevelModel };
