const mongoose = require('mongoose')

const propertyModel = mongoose.Schema({

	// personalInfo
	userId: {
		type: String,
		required: true
	},

	landlordContact: {
		type: String,
		required: true
	},
	landlordName: {
		type: String,
		required: true
	},

	// Property Info
	propertyType: {
		type: String,
		required: true
	},
	propertyLevel: {
		type: String,
		required: true
	},
	propertyLevelRate: {
		type: String,
		required: true
	},
	propertyPrice: {
		type: String,
		required: true
	},
	propertyDesc: {
		type: String,
		required: true
	},
	numberOfRooms: {
		type: String,
		required: true
	},
	numberOfWashrooms: {
		type: String,
		required: true
	},
	carPort: {
		type: Boolean,
		default: false
	},
	propertyImages: {
		type: Array,
		required: true
	},

	isRented: {
		type: Boolean,
		default: false
	},

	numberOfViews: {
		type: Number,
		default: 0
	},

	dateAdded: {
		type: String,
		required: true
	},

	// Location Info
	locationFromMap: {
		type: String,
		required: true
	},
	country: {
		type: String,
		required: true
	},
	region: {
		type: String,
		required: true
	},
	city: {
		type: String,
		required: true
	}

});

const Property = mongoose.model('Property', propertyModel);
module.exports = { Property };
