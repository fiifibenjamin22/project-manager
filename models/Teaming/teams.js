const mongoose = require('mongoose');

const teamSchema = mongoose.Schema({

	teamName: {
		type: String,
		required: true
	},

	teamPurpose: {
		type: String,
		required: true
	},

	teamMembers: {
		type: [],
		required: true
	},

	teamTask: {
		type: Object,
		required: true
	},

	dateCreated: {
		type: String,
		required: false
	},

	createdBy: {
		type: String,
		required: false
	}
});

const TeamModel = mongoose.model('Teams', teamSchema);
module.exports = { TeamModel };
