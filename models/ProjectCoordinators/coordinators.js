const mongoose = require('mongoose');

const coordinatorSchema = mongoose.Schema({

	coordinatorName: {
		type: String,
		required: true
	},

	mobile: {
		type: String,
		required: true
	},

	gender: {
		type: String,
		required: true
	},

	age: {
		type: String,
		required: true
	},

	location: {
		type: String,
		required: true
	}
});

const ProjectCoordinators = mongoose.model('Project Coordinators', coordinatorSchema);
module.exports = { ProjectCoordinators };
